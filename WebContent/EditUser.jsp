<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Insert title here</title>
	<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
<div id="sidebar">
	<div id="menu">
		<h1>小林舞子商店</h1>
		
		<div id="link">
			<a href="./">ホーム</a><br />
			<c:if test="${ loginUser.storeId == 1 && loginUser.positionId == 1 }">
				<a href="signup">ユーザー新規登録</a><br />
				<a href="userManagement">ユーザー管理画面</a><br />
			</c:if>
			<a href="logout">ログアウト</a>
		</div>
		
	</div>
</div>
<div id ="main">
	<div id="signupForm">
	<h1 id = "shopName">ユーザーの編集</h1>
		<c:if test="${ not empty errorMessages }">
			<div id="errorMessagesSignup">
					<c:forEach items="${errorMessages}" var="message">
						<c:out value="${message}" /><br />
					</c:forEach>
			</div>
			<c:remove var="errorMessages" scope="session"/>
		</c:if>
	
		<form action="editUser" method="post"><br />
			
			<label for="loginId">ログインID(半角英数字6文字以上20文字以下)</label>
			<input type="text" size="60" maxlength="20" name="loginId" value="${editUser.loginId}" id="loginId"/>　<br />
			
			<label for="password">パスワード(記号を含む半角英数字6文字以上20文字以下)</label>
			<input type="password" size="60" maxlength="20" name="password" id="password"/>　<br />
			
			<label for="passwordPre">パスワード(確認用)</label>
			<input type="password" size="60" maxlength="20" name="passwordPre" id="passwordPre"/>　<br />
			
			<label for="name">名前(10文字以下)</label>
			<input type="text" size="60" maxlength="10" name="name" value="${editUser.name}" id="name"/>　<br />
			
			<input name="userId" type="hidden" value="${editUser.id}" id="userId"/> <br />
		    
		    <!-- 他人のセレクトボックス -->
			
			<c:if test="${ loginUser.id != editUser.id}">
				<table>
					<tr>
						<td width="100">店舗</td>
						<td><select name="storeId">
								<c:forEach items="${stores}" var="store">
									<option	class = "store" value="${store.id}" <c:if test="${store.id == editUser.storeId}">selected</c:if>><c:out value="${store.name}" /></option>
								</c:forEach> 
					    	</select>
				    	</td>
				    </tr>
				    <tr>
						<td width="100">部署/役職</td>
						<td>
							<select name="positionId">
								<c:forEach items="${positions}" var="position">
									<option	class = "position" value="${position.id}" <c:if test="${position.id == editUser.positionId}">selected</c:if>><c:out value="${position.name}" /></option>
								</c:forEach>
							</select>
						</td>
					</tr>
				</table>
			</c:if>
				
			<!-- 自分のセレクトボックス -->
			<c:if test="${ loginUser.id == editUser.id}">
			
				店舗：<select name="storeId">
					<c:forEach items="${stores}" var="store">
						<c:if test="${ loginUser.storeId == store.id}">
							<option	class = "store" value="${store.id}" <c:if test="${store.id == editUser.storeId}">selected</c:if>><c:out value="${store.name}" /></option>
						</c:if>
					</c:forEach> 
		    	</select>
				<br />
				部署・役職：<select name="positionId">
					<c:forEach items="${positions}" var="position">
						<c:if test="${ loginUser.positionId == position.id}">
							<option	class = "position" value="${position.id}" <c:if test="${position.id == editUser.positionId}">selected</c:if>><c:out value="${position.name}" /></option>
						</c:if>
					</c:forEach>
				</select>
			</c:if>
		    <br />
			<input type="submit" value="更新する" /> <br />
		</form>
	</div>
	<div id="footer">Copyright(c)Kobayashi Maiko</div>
</div>	
</body>
</html>