$(function() {
	$('button#registerComment').on('click', function() {
		// 送信するデータを用意
		var form = $(this).parents("form#doComment");
		var userName = form.find('#userName').val();
		var userId = form.find('#userId').val();
		var newsId = form.find('#newsId').val();
		var text = form.find('#comment').val();
		var comment = { 'userName': userName , 'userId': userId, 'newsId': newsId, 'comment': text };
		console.log("Wow!");
		// 全てのerror-area-のメッセージを削除する
		$(".error-area-" + form.attr("data")).each(function(k, v){ $(v).empty(); })
		
		//$(".error-area").each(function(k, v){ $(v).find("ul").empty(); })

		// Ajax通信処理
		$.ajax({
			dataType: 'json',
			type: "POST",
			url: 'comment/register',
			data: { comment: JSON.stringify(comment) }
		}).done(function(data, textStatus, jqXHR) {
			// 成功時の処理
			if(data.is_success == 'true') {
				/* バリデーション通過 */
				//console.log(data.createdAt + "と" + data.commentId);
				console.log(data.createdAt + "と" + data.commentId);
				//Dataを希望のフォーマットにする
				// 非同期で表示したいデータをhtmlにappend
				form.parents("div.article").find("div.allComment-" + form.attr("data")).append(
						"<br /><table><tr><td>" + userName + "</td></tr></table>"+
						"<table><tr><td bgcolor='#E6E6FA' style='padding : 10px;'>" + text + "</td></tr></table>"+
						"<table><tr><td valign='bottom'><div class='date'>" + data.createdAt + "</div></td></tr></table>"+
						"<form  action='deleteComment' method='post'>"+
						"<input type='hidden' name='commentId' value='" + data.commentId +"'/>"+
						"<input type='submit' value='コメント削除' onClick='return deleteComment()'><br />");
				form.parents("div.article").find("div.allComment-" + form.attr("data")).append("</form>");
				console.log(userName +":"+ text);
				// 全てのtextarea要素の中身を削除
				$(".comment").each(function(k, v) { $(this).val(""); })
			} else {
				/* バリデーションエラー */
				console.log("ValidationError!");
				var errorArea = form.parents("div.article").find("div.error-area-" + form.attr("data"));
				// 入力フォームの上にエラーメッセージを表示させる
				data.errors.forEach(function(v, k) {
					// エラーメッセージの要素の数だけ表示させる
					errorArea.append("<div class='commentErrorMessages'>" + v + "</div>");
				});
			}
		}).fail(function(data, textStatus, jqXHR) {
			// 通信失敗時の処理
			console.log(data);
			console.log('error!!');
		});
	});
});
