<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>記事の投稿</title>
	<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
<div id="sidebar">
	<div id="menu">
		<h1>小林舞子商店</h1>
		
		<div id="link">
			<a href="./">ホーム</a><br />
			<c:if test="${ loginUser.storeId == 1 && loginUser.positionId == 1 }">
				<a href="signup">ユーザー新規登録</a><br />
				<a href="userManagement">ユーザー管理画面</a><br />
			</c:if>
			<a href="logout">ログアウト</a>
		</div>
		
	</div>
</div>
<div id ="main">
	<div id="signupForm">
	<h1 id = "shopName">記事を書く！</h1>
		<c:if test="${ not empty errorMessages }">
			<div id="errorMessagesSignup">
					<c:forEach items="${errorMessages}" var="message">
						<c:out value="${message}" /><br />
					</c:forEach>
			</div>
			<c:remove var="errorMessages" scope="session"/>
		</c:if>
		<br />
		<form action="postNews" method="post">
			<label for="title">タイトル(30文字以下)</label>
			<input type="text" class="newsText" size="60" maxlength="30" name="title" value="${news.title}" id="title"/> <br />
			
			<label for="category">カテゴリ(10文字以下)</label>
			<input type="text" class="newsText" size="60" maxlength="10" name="category" value="${news.category}" id="category"/> <br />
			
			本文(1000文字以下)<br />
			<textarea name="text" id="newsText" onkeyup="document.getElementById('countNews').value=this.value.length">${news.text}</textarea>
			<br />
			入力中の文字数:<input type="text" size="4" maxlength="4" id="countNews">
			<br />
			<br />
			<input type="submit" value="投稿する">
		</form>
	</div>
	<div id="footer">Copyright(c)Kobayashi Maiko</div>
</div>
</body>
</html>