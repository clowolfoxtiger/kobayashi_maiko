<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ホーム</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<script src="//code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="./js/ajax.js"></script>
<script type="text/javascript">

	function deleteNews(){
		if(window.confirm('投稿を削除しますか？')){
			location.href = "deleteNews";
		}else{
			window.alert('投稿削除をキャンセルしました');
			return false;
		}
	}
	
	function deleteComment(){
		if(window.confirm('コメントを削除しますか？')){
			location.href = "deleteComment";
		}else{
			window.alert('コメント削除をキャンセルしました');
			return false;
		}
	}
	
 	function countLength( text, field ) {
        document.getElementById(field).innerHTML = "入力文字数:" + text.length;
    }

</script>
	
</head>

<body>
<div id="sidebar">
    
	<div id="menu">
		<h1>小林舞子商店</h1>
		<h1><a href="postNews">投稿する</a></h1>
		<div id="link">
			<c:if test="${ not empty loginUser }">
				<a href="./">ホーム</a><br />
				<c:if test="${ loginUser.storeId == 1 && loginUser.positionId == 1 }">
					<a href="signup">ユーザー新規登録</a><br />
					<a href="userManagement">ユーザー管理画面</a><br />
				</c:if>
				<a href="logout">ログアウト</a>
			</c:if>
		</div>
		<br />
		<h4>記事の検索</h4>
		<div id = "narrowing">
			<form action="./" method="get">
				<div>
					<input type="date" name="dateStart" value="${dateStart}" id="dateStart">から
					<input type="date" name="dateEnd" value="${dateEnd}" id="dateEnd">まで
			    	<br /><br />
			    	カテゴリ<br />
				    <input type="text" name="category" value="${category}" id="category"/>
				    <br /><br />
				    <input type="submit" value="絞り込む"><br />
				</div>  
			</form>
			<form action="./" method="get">
				<input type="submit" value="リセット"><br />
			</form>  
		</div>
	</div>
</div>

<div id="main">
	<div id="main-contentsTop">
		<c:if test="${ not empty errorMessages }">
			<div id="errorMessagesNarrow">
				<c:forEach items="${errorMessages}" var="message">
					<c:out value="${message}" />
				</c:forEach>
			</div>
			<c:remove var="errorMessages" scope="session"/>
		</c:if>
		
		<div class ="article">
			<c:forEach items="${userNews}" var="userNews">
				<br /><br />
				<div class = "news">	
					<div class="postNews">
					
						<!-- TABLEニュース -------------------------------------------------->
						<table style="border-color:#1E90FF; border-style: solid;" bgcolor="#ffffff" id="topNewsTable">
							<tr>
								<td align="left" width="70%" style="padding-left: 30px;"><div class="title"><h3><c:out value="${userNews.title}" /></h3></div></td>
								<td align="right" valign="bottom" style="padding-right: 30px;"><div class="name"><h4><c:out value="${userNews.name}" /></h4></div></td>
							</tr>
							<tr>		
								<td align="left" width="70%" style="padding-left: 10px;"><div class="category">Category:<c:out value="${userNews.category}" /></div></td>
								<td align="right" valign="bottom" style="padding-right: 30px;"><div class="date"><fmt:formatDate value="${userNews.createdAt}" pattern="yyyy/MM/dd/HH:mm:ss" /></div></td>
							</tr>
						</table>
						
						<br />
						<div id="topNewsText"><c:forEach var="n" items="${fn:split(userNews.text, '
')}"><!-- ここ改行のために必要 -->
					    	<div>${n}</div>
						</c:forEach></div>
					</div>
					
					<c:if test="${ not empty loginUser }">
						<c:if test="${loginUser.id == userNews.userId}">
							<form action="deleteNews" method="post">
								<input type="hidden" name="newsId" value="${userNews.newsId}" id="newsId"/>
								<input type="submit" value="投稿削除" onClick="return deleteNews()"><br />
							</form>	
						</c:if>
					</c:if>
				</div>
				
				
				<div class ="allComment-${userNews.newsId}">
					<c:forEach items="${userComments}" var="userComment">
						<c:if test="${userNews.newsId == userComment.newsId}">
							<br />
							
							<table>
								<tr>
									<td><c:out value="${userComment.name}" /></td>
								</tr>
							</table>
							<table>
								<tr>
									<td bgcolor="#E6E6FA" style="padding : 10px;">
										<c:forEach var="s" items="${fn:split(userComment.comment, '
')}">
									    	<div>${s}</div>
										</c:forEach>
									</td>
								</tr>
							</table>
							<table>
								<tr>
									<td valign="bottom"><div class="date"><fmt:formatDate value="${userComment.createdAt}" pattern="yyyy/MM/dd/HH:mm:ss" /></div></td>
								</tr>
							</table>
			
							<c:if test="${ not empty loginUser }">
								<c:if test="${loginUser.id == userComment.userId}">
									<form action="deleteComment" method="post">
										<input type="hidden" name="commentId" value="${userComment.commentId}" id="newsId"/>
										<input type="submit" value="コメント削除" onClick="return deleteComment()"><br />
									</form>	
								</c:if>
							</c:if>	
						</c:if>
					</c:forEach>
				</div>
				<br />
		
				<div class='error-area-${userNews.newsId}'>
		      	</div>
		      	
				<form id="doComment" data="${userNews.newsId}">
					<input type="hidden" name="userName" value="${loginUser.name}" id="userName"/>
					<input type="hidden" name="userId" value="${loginUser.id}" id="userId"/>
					<input type="hidden" name="newsId" value="${userNews.newsId}" id="newsId"/>
					<textarea id ="comment" name="comment" cols="100" rows="5"  onkeyup="countLength(value, 'showComment${userNews.newsId}');"><c:if test="${ not empty comment && comment.newsId == userNews.newsId }"><c:out value="${comment.comment}" /></c:if></textarea>
					<p id="showComment${userNews.newsId}" >入力文字数:0</p>
					<button id ="registerComment" type="button">コメントする</button> (500文字以下)
				</form>	
				<br /><br /><br /><br />
			</c:forEach>	
		</div>
	</div>
	<div id="footer">Copyright(c)Kobayashi Maiko</div>
</div>	
</body>
</html>
