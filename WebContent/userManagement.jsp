<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ユーザー一覧</title>
	<link href="./css/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript">

function stop(){
	if(window.confirm('停止しますか？')){
		location.href = "stopUser";
	}else{
		window.alert('停止をキャンセルしました');
		return false;
	}
}

function revive(){
	if(window.confirm('復活しますか？')){
		location.href = "reviveUser";
	}
	else{
		window.alert('復活をキャンセルしました');
		return false;
	}
}

</script>
</head>
<body>

<div id="sidebar">
	<div id="menu">
		<h1>小林舞子商店</h1>
		
		<div id="link">
			<a href="./">ホーム</a><br />
			<c:if test="${ loginUser.storeId == 1 && loginUser.positionId == 1 }">
				<a href="signup">ユーザー新規登録</a><br />
				<a href="userManagement">ユーザー管理画面</a><br />
			</c:if>
			<a href="logout">ログアウト</a>
		</div>
		
	</div>
</div>

<div id="main">
	<div id="userManagementForm">
		<div id = "title-name">
			<h1 id ="shopName">ユーザー一覧</h1>
		</div>
		<c:if test="${ not empty errorMessages }">
			<div id="errorMessagesUserManagement">
				<c:forEach items="${errorMessages}" var="message">
					<c:out value="${message}" />
				</c:forEach>
			</div>
			<c:remove var="errorMessages" scope="session"/>
		</c:if>
		
		<div class = "soumuJinnji">
			<h2><総務人事></h2>		
			<c:forEach items="${usersManagement}" var="userManagement">
				<c:if test = "${userManagement.positionId == 1 }">
					<table style="border-color:#1E90FF; border-style: solid;" width="800" height="80">
						<tr>
							<td width="150" align="center">
								<div class="name"><c:out value="${userManagement.userName}" /></div>
								
							</td>
							<td width="200">
								<div class="loginId">ログインID: <c:out value="${userManagement.loginId}" /></div>
								<c:out value="${userManagement.storeName}" /> / <c:out value="${userManagement.positionName}" />
								<div class="updatedAt"><fmt:formatDate value="${userManagement.updatedAt}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
							</td>
							<td width="80" align="center">
								<form action="editUser" method="get">
									<input type="hidden" name="userId" value="${userManagement.userId}" id="userId"/>
									<input type="submit" value="編集する" id="editButton"><br />
								</form>
							</td>
							<td width="80" align="center">
								<c:if test="${ userManagement.isDeleted == 0}">
									<form action="stopUser" method="post">
										<input type="hidden" name="userId" value="${userManagement.userId}" id="userId"/>
										<input type="submit" value="停止する" onClick="return stop()" id="stopButton">
									</form>
								</c:if>
								
								<c:if test="${ userManagement.isDeleted == 1}">
									<form action="reviveUser" method="post">
										<input type="hidden" name="userId" value="${userManagement.userId}" id="userId"/>
										<input type="submit" value="復活する" onClick="return revive()" id="reviveButton">
									</form>
								</c:if>
							</td>
						</tr>
					</table>
					<br />
				</c:if>
			</c:forEach>
			<br />
		</div>
		
		<div class = "jyouhouKannri">
			<h2><情報管理></h2>
			<c:forEach items="${usersManagement}" var="userManagement">
				<c:if test = "${userManagement.positionId == 2 }">
					<table style="border-color:#1E90FF; border-style: solid;" width="800" height="80">
						<tr>
							<td width="150" align="center">
								<div class="name"><c:out value="${userManagement.userName}" /></div>
								
							</td>
							<td width="200">
								<div class="loginId">ログインID: <c:out value="${userManagement.loginId}" /></div>
								<c:out value="${userManagement.storeName}" /> / <c:out value="${userManagement.positionName}" />
								<div class="updatedAt"><fmt:formatDate value="${userManagement.updatedAt}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
							</td>
							<td width="80" align="center">
								<form action="editUser" method="get">
									<input type="hidden" name="userId" value="${userManagement.userId}" id="userId"/>
									<input type="submit" value="編集する" id="editButton"><br />
								</form>
							</td>
							<td width="80" align="center">
								<c:if test="${ userManagement.isDeleted == 0}">
									<form action="stopUser" method="post">
										<input type="hidden" name="userId" value="${userManagement.userId}" id="userId"/>
										<input type="submit" value="停止する" onClick="return stop()" id="stopButton">
									</form>
								</c:if>
								
								<c:if test="${ userManagement.isDeleted == 1}">
									<form action="reviveUser" method="post">
										<input type="hidden" name="userId" value="${userManagement.userId}" id="userId"/>
										<input type="submit" value="復活する" onClick="return revive()" id="reviveButton">
									</form>
								</c:if>
							</td>
						</tr>
					</table>
					<br />
				</c:if>
			</c:forEach>
			<br />
		</div>
		
		<div class = "shitenncho">	
			<h2><支店長></h2>
			<c:forEach items="${usersManagement}" var="userManagement">
				<c:if test = "${userManagement.positionId == 3 }">
					<table style="border-color:#1E90FF; border-style: solid;" width="800" height="80">
						<tr>
							<td width="150" align="center">
								<div class="name"><c:out value="${userManagement.userName}" /></div>
								
							</td>
							<td width="200">
								<div class="loginId">ログインID: <c:out value="${userManagement.loginId}" /></div>
								<c:out value="${userManagement.storeName}" /> / <c:out value="${userManagement.positionName}" />
								<div class="updatedAt"><fmt:formatDate value="${userManagement.updatedAt}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
							</td>
							<td width="80" align="center">
								<form action="editUser" method="get">
									<input type="hidden" name="userId" value="${userManagement.userId}" id="userId"/>
									<input type="submit" value="編集する" id="editButton"><br />
								</form>
							</td>
							<td width="80" align="center">
								<c:if test="${ userManagement.isDeleted == 0}">
									<form action="stopUser" method="post">
										<input type="hidden" name="userId" value="${userManagement.userId}" id="userId"/>
										<input type="submit" value="停止する" onClick="return stop()" id="stopButton">
									</form>
								</c:if>
								
								<c:if test="${ userManagement.isDeleted == 1}">
									<form action="reviveUser" method="post">
										<input type="hidden" name="userId" value="${userManagement.userId}" id="userId"/>
										<input type="submit" value="復活する" onClick="return revive()" id="reviveButton">
									</form>
								</c:if>
							</td>
						</tr>
					</table>
					<br />
				</c:if>
			</c:forEach>
			<br />
		</div>
		
		<div class = "shainn">	
			<h2><社員></h2>
			<c:forEach items="${usersManagement}" var="userManagement">
				<c:if test = "${userManagement.positionId == 4 }">
					<table style="border-color:#1E90FF; border-style: solid;" width="800" height="80">
						<tr>
							<td width="150" align="center">
								<div class="name"><c:out value="${userManagement.userName}" /></div>
								
							</td>
							<td width="200">
								<div class="loginId">ログインID: <c:out value="${userManagement.loginId}" /></div>
								<c:out value="${userManagement.storeName}" /> / <c:out value="${userManagement.positionName}" />
								<div class="updatedAt"><fmt:formatDate value="${userManagement.updatedAt}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
							</td>
							<td width="80" align="center">
								<form action="editUser" method="get">
									<input type="hidden" name="userId" value="${userManagement.userId}" id="userId"/>
									<input type="submit" value="編集する" id="editButton"><br />
								</form>
							</td>
							<td width="80" align="center">
								<c:if test="${ userManagement.isDeleted == 0}">
									<form action="stopUser" method="post">
										<input type="hidden" name="userId" value="${userManagement.userId}" id="userId"/>
										<input type="submit" value="停止する" onClick="return stop()" id="stopButton">
									</form>
								</c:if>
								
								<c:if test="${ userManagement.isDeleted == 1}">
									<form action="reviveUser" method="post">
										<input type="hidden" name="userId" value="${userManagement.userId}" id="userId"/>
										<input type="submit" value="復活する" onClick="return revive()" id="reviveButton">
									</form>
								</c:if>
							</td>
						</tr>
					</table>
					<br />
				</c:if>
			</c:forEach>
			<br />
		</div>
	</div>
	<div id="footer">Copyright(c)Kobayashi Maiko</div>
</div>

</body>
</html>