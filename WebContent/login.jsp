<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ログイン</title>
	<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body id="loginBody">
<div id="login">
	<h1 id = "loginTitle">小林舞子商店</h1><br />
	<div id="main-contentsLogin">
		
		<form action="login" method="post"><br />
			<label for="loginId">ログインID</label>
			<input name="loginId" value="${loginId}" id="loginId"/> <br /><br />
		
			<label for="password">パスワード</label>
			<input name="password" type="password" id="password"/> <br /><br />
		
			<input type="submit" value="ログイン" /> <br /><br />
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<c:forEach items="${errorMessages}" var="message">
						<c:out value="${message}" />
					</c:forEach>
				</div>
				<c:remove var="errorMessages" scope="session"/>
			</c:if>
		</form>
	</div>
	<br/>
</div>
<div id="footer">Copyright(c)Kobayashi Maiko</div>
</body>
</html>
