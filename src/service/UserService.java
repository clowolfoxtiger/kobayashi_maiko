package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.sql.Connection;

import org.apache.commons.lang.StringUtils;

import beans.User;
import dao.UserDao;
import utils.CipherUtil;
import utils.StreamUtil;

public class UserService {

	public void register(User user) {/////////////////////////////////////////////////

		Connection connection = null;
		try {
			connection = getConnection();

			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);

			setDefaultIcon(user);

			UserDao userDao = new UserDao();
			userDao.insert(connection, user);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	private void setDefaultIcon(User user) {

		InputStream is = null;
		try {
			long randomNum = System.currentTimeMillis() % 5;
			String filePath = "/duke_" + randomNum + ".jpg";
			is = UserService.class.getResourceAsStream(filePath);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			StreamUtil.copy(is, baos);
			//user.setIcon(baos.toByteArray());
		} finally {
			close(is);
		}
	}

	public void update(User editUser) {//////////////////////////////////////////////////////////////////

		Connection connection = null;
		try {
			connection = getConnection();

			if(!StringUtils.isEmpty(editUser.getPassword())){
				String encPassword = CipherUtil.encrypt(editUser.getPassword());
				editUser.setPassword(encPassword);
			}
			System.out.println(editUser.getPassword());

			UserDao userDao = new UserDao();
			userDao.update(connection, editUser);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	
	//ユーザー停止する　management
	public void updateOne(User stopUser) {//////////////////////////////////////////////////////

		Connection connection = null;
		try {
			connection = getConnection();

			
			System.out.println(stopUser.getId());

			UserDao userDao = new UserDao();
			userDao.updateOne(connection, stopUser);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	
	//ユーザー復活する management
		public void updateZero(User reviveUser) {/////////////////////////////////////////

			Connection connection = null;
			try {
				connection = getConnection();

				UserDao userDao = new UserDao();
				userDao.updateZero(connection, reviveUser);

				commit(connection);
			} catch (RuntimeException e) {
				rollback(connection);
				throw e;
			} catch (Error e) {
				rollback(connection);
				throw e;
			} finally {
				close(connection);
			}
		}

	public User getUser(String loginId, String password) {

		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			User user = userDao.getUser(connection, loginId, password);

			commit(connection);

			return user;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
