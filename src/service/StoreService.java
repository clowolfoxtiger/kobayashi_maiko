package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Store;
import dao.StoreDao;

//storeテーブルにコネクト
public class StoreService {
	
	public void register(Store store) {

		Connection connection = null;
		try {
			connection = getConnection();

			StoreDao storeDao = new StoreDao();
			storeDao.insert(connection, store);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	

	public List<Store> getName() {

		Connection connection = null;
		try {
			connection = getConnection();

			StoreDao storeDao = new StoreDao();
			List<Store> ret = storeDao.getStore(connection);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
