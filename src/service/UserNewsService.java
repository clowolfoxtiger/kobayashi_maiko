package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.UserNews;
import dao.UserNewsDao;

public class UserNewsService {
	
	public List<UserNews> getUserNews(String dateStart, String dateEnd, String category) {///使ってる

		Connection connection = null;
		try {
			connection = getConnection();

			UserNewsDao userNewsDao = new UserNewsDao();
			List<UserNews> ret = userNewsDao.getUserNews(connection, dateStart, dateEnd, category);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
