package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Comento;
import beans.UserComment;
import dao.CommentDao;


public class CommentService {

	public void register(Comento comment) {/////////////////////////////////////

		Connection connection = null;
		try {
			connection = getConnection();

			CommentDao commentDao = new CommentDao();
			commentDao.insert(connection, comment);

			commit(connection);
			
			
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	
	//ajax　コメント情報とってくる
	public Comento registerAjax(Comento comment) {///////////////////////////////

		Connection connection = null;
		try {
			connection = getConnection();

			CommentDao commentDao = new CommentDao();
			//インサートして且つ、インサートしたAutoIncrementIDを取る
			int id =commentDao.insertAjax(connection, comment);
			//コメントクラス情報丸々取得
			Comento commentClass = commentDao.getCommentClass(connection, id);
			
			commit(connection);
			
			return commentClass;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
			
		}
	}
	

	
	private static final int LIMIT_NUM = 1000;
	
	public List<UserComment> getUserComments() {////使ってる
	
		Connection connection = null;
		try {
			connection = getConnection();
	
			CommentDao commentDao = new CommentDao();
			List<UserComment>ret = commentDao.getUserComments(connection, LIMIT_NUM);
	
			commit(connection);
	
			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	

	public void delete(Comento comment, String commentId) {////////////////////////////

		Connection connection = null;
		try {
			connection = getConnection();

			CommentDao commentDao = new CommentDao();
			commentDao.delete(connection, comment, commentId);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	
	//news削除に伴う全コメント削除
	public void deleteAllComment(Comento comment, String newsId) {/////////////////

		Connection connection = null;
		try {
			connection = getConnection();

			CommentDao commentDao = new CommentDao();
			commentDao.deleteAllComment(connection, comment, newsId);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
