package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.News;
import dao.NewsDao;
import exception.SQLRuntimeException;


public class NewsService {
	
	public void register(News news) {//////////////////////////////////////////////

		Connection connection = null;
		try {
			connection = getConnection();

			NewsDao newsDao = new NewsDao();
			newsDao.insert(connection, news);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	
	public List<News> getNews(Connection connection, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM news ");
			sql.append("ORDER BY updated_at DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<News> ret = toNewsList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}


	private List<News> toNewsList(ResultSet rs)
			throws SQLException {

		List<News> ret = new ArrayList<News>();
		try {
			while (rs.next()) {
				String title = rs.getString("title");
				String category = rs.getString("category");
				String text = rs.getString("text");
				int userId = rs.getInt("user_id");
				Timestamp createdAt = rs.getTimestamp("created_at");
				Timestamp updatedAt = rs.getTimestamp("updated_at");

				News news = new News();
				news.setTitle(title);
				news.setCategory(category);
				news.setText(text);
				news.setUserId(userId);
				news.setCreatedAt(createdAt);
				news.setUpdatedAt(updatedAt);

				ret.add(news);
			}
			return ret;
		} finally {
			close(rs);
		}
	}



	public List<News> getNews() {

		Connection connection = null;
		try {
			//これから先の接続情報取得
			connection = getConnection();
			
			//Daoを使えるようにする
			NewsDao newsDao = new NewsDao();
			//Daoの中のgetNewsメソッドを呼び出す
			List<News> ret = newsDao.getNews(connection);
			
			//受け取った処理が確定
			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	
	
	//newsデリート
	public void delete(News news, String newsId) {/////使ってる

		Connection connection = null;
		try {
			connection = getConnection();

			NewsDao newsDao = new NewsDao();
			newsDao.delete(connection, news, newsId);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}


}
