package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.User;
import beans.UserManagement;
import dao.ManagementDao;

public class ManagementService {
	
	
	public List<UserManagement> getUserManagements() {
	
		Connection connection = null;
		try {
			connection = getConnection();
	
			ManagementDao managementDao = new ManagementDao();
			List<UserManagement>ret = managementDao.getUserManagements(connection);
	
			commit(connection);
	
			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	
	public User getUser(String userId) {/////////////////////////////////////////////////////
		
		Connection connection = null;
		try {
			connection = getConnection();
	
			ManagementDao managementDao = new ManagementDao();
			User user = managementDao.getUser(connection, userId);
	
			commit(connection);
			return user;
			
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	
public User getLoginIdCheckedUser(String loginId) {/////////////////////////////////////////////////////
		
		Connection connection = null;
		try {
			connection = getConnection();
	
			ManagementDao managementDao = new ManagementDao();
			User user = managementDao.getLoginIdCheckedUser(connection, loginId);

	
			commit(connection);
			return user;
			
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	

}
