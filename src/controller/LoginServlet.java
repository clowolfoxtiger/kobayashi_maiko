package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import service.LoginService;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		request.getRequestDispatcher("/login.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		
		LoginService loginService = new LoginService();
		User user = loginService.login(loginId, password);
		HttpSession session = request.getSession();
		if (user != null && user.getIsDeleted() == 0) {
			System.out.println("OK");
			session.setAttribute("loginUser", user);
			
			//ホーム画面へ遷移
			response.sendRedirect("./");
		}else{
			if(user != null && user.getIsDeleted() == 1){
					//エラーメッセージ
					System.out.println("NO1");
					List<String> messages = new ArrayList<String>();
					messages.add("停止中のユーザーです");
					session.setAttribute("errorMessages", messages);
			}
			if(user == null){
				System.out.println("NO2");
				//エラーメッセージ
				List<String> messages = new ArrayList<String>();
				messages.add("ログインに失敗しました");
				session.setAttribute("errorMessages", messages);
			}
			//エラーメッセージを出すページへ移った時に、入力値が消えないようにする
			request.setAttribute("loginId", loginId);
			request.getRequestDispatcher("login.jsp").forward(request, response);
		}
	}
}
