package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Comento;
import service.CommentService;

@WebServlet(urlPatterns = { "/deleteComment" })

public class DeleteCommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		
		//エラーメッセージ無かったら、delete
		System.out.println("☆☆☆");
		//指定したコメントを削除
		///ここクラス変えたよ！！！
		Comento comment = new Comento();
		String commentId = request.getParameter("commentId");
		//System.out.println(commentId);
		new CommentService().delete(comment, commentId);
		
		//ホーム画面へredirect
		response.sendRedirect("./");
			
	}

}