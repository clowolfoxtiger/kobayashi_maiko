package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Position;
import beans.Store;
import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import service.ManagementService;
import service.PositionService;
import service.StoreService;
import service.UserService;

@WebServlet(urlPatterns = { "/editUser" })
public class EditUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest request,HttpServletResponse response) 
	throws IOException, ServletException {
		
		String userId = request.getParameter("userId");
		
		ManagementService managementService = new ManagementService();
		List<String> messages = new ArrayList<String>();
		
		//編集したいユーザーの内容
		User editUser = managementService.getUser(userId);		
		request.setAttribute("editUser", editUser );
		
		//storeテーブルの情報をget
		List<Store> stores = new StoreService().getName();
		request.setAttribute("stores", stores);
		
		//positionテーブルの情報をget
		List<Position> positions = new PositionService().getName();
		request.setAttribute("positions", positions);
		//System.out.println(positions.size());
		
		//ここで存在しないユーザーのIDを弾く
		if(userId == null || userId == ""){
			messages.add("存在しないユーザーIDが入力されました");
			request.getSession().setAttribute("errorMessages", messages);
			response.sendRedirect("userManagement");
		}else if(!(userId.matches("^[0-9]+$")) || editUser == null){
			messages.add("存在しないユーザーIDが入力されました");
			request.getSession().setAttribute("errorMessages", messages);
			response.sendRedirect("userManagement");
		}else{
			//この（ユーザー編集）画面へforwardして情報を見せる
			request.setAttribute("editUser", editUser);
			request.getRequestDispatcher("EditUser.jsp").forward(request, response);
		}
	
	}
	
	//アップデートするボタン押されたら
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		///
		List<String> messages = new ArrayList<String>();
	
		HttpSession session = request.getSession();
		User editUser = getEditUser(request);
		List<Store> stores = new StoreService().getName();
		List<Position> positions = new PositionService().getName();
		
		if (isValid(request, messages, editUser) == true) {
	
			try {
				new UserService().update(editUser);
				User loginUser = (User)session.getAttribute("loginUser");
				if( editUser.getId() == loginUser.getId()){
					session.setAttribute("loginUser", editUser);
				}
			} catch (NoRowsUpdatedRuntimeException e) {
				session.removeAttribute("editUser");
				messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
				session.setAttribute("errorMessages", messages);
				response.sendRedirect("settings");
			}
	
			response.sendRedirect("userManagement");
		} else {
			
			session.setAttribute("errorMessages", messages);
			request.setAttribute("editUser", editUser);
			request.setAttribute("stores", stores);
			request.setAttribute("positions", positions);
			request.getRequestDispatcher("EditUser.jsp").forward(request, response);
		}
	}

	private User getEditUser(HttpServletRequest request)
			throws IOException, ServletException {
	
		User editUser = new User();
		
		editUser.setName(request.getParameter("name"));
		editUser.setLoginId(request.getParameter("loginId"));
		editUser.setPassword(request.getParameter("password"));
		editUser.setStoreId(Integer.parseInt(request.getParameter("storeId")));
		editUser.setPositionId(Integer.parseInt(request.getParameter("positionId")));
		editUser.setId(Integer.parseInt(request.getParameter("userId")));
		
		return editUser;
	}
	
	private boolean isValid(HttpServletRequest request, List<String> messages, User editUser) {
	
		String name = request.getParameter("name");
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String passwordPre = request.getParameter("passwordPre");
		String storeId = request.getParameter("storeId");
		String positionId = request.getParameter("positionId");
		
		if (StringUtils.isEmpty(loginId) == true) {
			messages.add("ログインIDを入力してください");
		}else if(!(loginId.matches("^[a-zA-Z0-9]+$"))){
			messages.add("ログインIDを半角英数字で入力してください");
		}else if (loginId.length() < 6 || 20 < loginId.length()) {
			messages.add("ログインIDを6文字以上20文字以下で入力してください");
		}
		
		//同じloginIdを持つユーザーがいないか確認
		ManagementService managementService = new ManagementService();
		User loginIdCheckedUser = managementService.getLoginIdCheckedUser(loginId);
		if(loginIdCheckedUser != null && editUser.getId() != loginIdCheckedUser.getId()){
			messages.add("!登録済みのログインID! ("+ loginIdCheckedUser.getName() + "さんのログインIDです)");
		}
		
		if (StringUtils.isNotEmpty(password) == true) {
			if(!(password.matches("^(?=.*?[0-9a-zA-Z])(?=.*?[!-/@_])[A-Za-z!-9@_]+$"))){
				messages.add("パスワードを記号を含む半角英数字で入力してください");
			}else if (password.length() < 6 || 20 < password.length()) {
				messages.add("パスワードを6文字以上20文字以下で入力してください");
			}
		}
		
		if (!(password.equals(passwordPre))) {
			messages.add("パスワードと確認用パスワードが同一ではありません。");
		}
		
		if (StringUtils.isEmpty(name) == true) {
			messages.add("名前を入力してください");
		}else if (name.length() > 10) {
			messages.add("名前を10文字以下で入力してください");
		}
		
		if (StringUtils.isEmpty(storeId) == true) {
			messages.add("支店を選択してください");
		}
		if (StringUtils.isEmpty(positionId) == true) {
			messages.add("部署・役職名を選択してください");
		}
		
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
	
	
	

}
