package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import service.ManagementService;
import service.UserService;

@WebServlet(urlPatterns = { "/reviveUser" })
public class ReviveUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();
		String userId = request.getParameter("userId");
		HttpSession session = request.getSession();
		
			try {
				ManagementService managementService = new ManagementService();
				User reviveUser = managementService.getUser(userId);	
				request.setAttribute("reviveUser", reviveUser );
				new UserService().updateZero(reviveUser);
				
			} catch (NoRowsUpdatedRuntimeException e) {
				session.removeAttribute("stopUser");
				messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
				response.sendRedirect("userManagement");
			}
			response.sendRedirect("userManagement");

	}

}
