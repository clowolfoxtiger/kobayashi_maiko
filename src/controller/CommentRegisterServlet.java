package controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

import beans.Comento;
import service.CommentService;

@WebServlet(urlPatterns = { "/comment/register" })
public class CommentRegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		//jarファイルの元のソースが見れない：課題
		
		//Json型のcommentをJavaで使えるようにした
		Comento comment = createCommentData(request);//comment,syso,beans.Comento@effc7fなんやこれ
		List<String> errorMessages = new ArrayList<String>();

		boolean isSuccess = false;
		String responseJson = "";
		String errors = "";
		//System.out.println(isValid(comment, errorMessages));
		if(isValid(comment, errorMessages)) {
			// バリデーション成功
			//System.out.println("newsId = " + comment.getNewsId());
			//System.out.println("comment = " + comment.getComment());
			// 成功情報を返す
			//コメントをインサート
			Comento commentClass = new CommentService().registerAjax(comment);///ここでエラー
			int commentId = commentClass.getId();
			Date createdAT = commentClass.getCreatedAt();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String createdAt = sdf.format(createdAT);
			isSuccess = true;
			responseJson = String.format("{\"is_success\" : \"%s\", \"commentId\" : \"%s\", \"createdAt\" : \"%s\"}", isSuccess, commentId, createdAt);
		} else {
			// バリデーションエラー
			// ListをJsonの形にして返す
			//System.out.println(errorMessages);
			errors = new ObjectMapper().writeValueAsString(errorMessages);
			responseJson = String.format("{\"is_success\" : \"%s\", \"errors\" : %s}", isSuccess, errors);
		}
		response.setContentType("application/json;charset=UTF-8");//ここも分からん
		response.getWriter().write(responseJson);
	}

	/*
	 * Json文字列をJavaオブジェクトに変換するメソッド
	 */
	private Comento createCommentData(HttpServletRequest request) {
		//sessionからloginUserのIdをとってComentoクラスに入れる
		
		String data = request.getParameter("comment");//jsの変数
		//System.out.println(data);//newsId":"5","commentText":"１１１"}
		Comento comment = new Comento();
		try {
			//Comentoクラスにdata値を入れた ここにcreatedAt 入れる？
			comment = new ObjectMapper().readValue(data, Comento.class);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return comment;
	}
	

	/*
	 * 入力データの検証を行うメソッド
	 */
	private boolean isValid(Comento comment, List<String> errorMessages) {
		if(StringUtils.isBlank(comment.getComment())) {
			errorMessages.add("コメントを入力してください");
		} else if(comment.getComment().length() > 500) {
			errorMessages.add("コメントは500文字以下で入力してください");
		}

		if(errorMessages.size() > 0) {
			return false;
		} else {
			return true;
		}
	}

}
