package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.News;
import beans.User;
import service.NewsService;

@WebServlet(urlPatterns = { "/postNews" })

public class PostNewsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		
		request.getRequestDispatcher("postNews.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		
		//エラーメッセージをListに入れる
		List<String> messages = new ArrayList<String>();
		News news = new News();
		//jspに入った値をset
		news.setTitle(request.getParameter("title"));
		news.setCategory(request.getParameter("category"));
		news.setText(request.getParameter("text"));
		User loginUser = (User)session.getAttribute("loginUser");
		news.setUserId(loginUser.getId());
		System.out.println(loginUser);
		
		if (isValid(request, messages) == true) {
			
			//データを登録
			new NewsService().register(news);

			//ホームに遷移
			response.sendRedirect("./");
		} else {
			request.setAttribute("errorMessages", messages);
			request.setAttribute("news", news);
			request.getRequestDispatcher("postNews.jsp").forward(request, response);
			
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages){
		
		String title = request.getParameter("title");
		String category = request.getParameter("category");
		String text = request.getParameter("text");
		
		if (StringUtils.isBlank(title) == true) {
			messages.add("タイトルを入力してください");
		}else if (30 < title.length()) {
			messages.add("タイトルを30文字以下で入力してください");
		}
		
		if (StringUtils.isBlank(category) == true) {
			messages.add("カテゴリを入力してください");
		}else if (10 < category.length()) {
			messages.add("カテゴリを10文字以下で入力してください");
		}
		
		if (StringUtils.isBlank(text) == true) {
			messages.add("本文を入力してください");
		}else if (1000 < text.length()) {
			messages.add("本文を1000文字以下で入力してください");
		}
		System.out.println(messages);
		
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}

