package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.Position;
import beans.Store;
import beans.User;
import service.ManagementService;
import service.PositionService;
import service.StoreService;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		
		//storeテーブルの情報をget
		List<Store> stores = new StoreService().getName();
		request.setAttribute("stores", stores);
		
		//positionテーブルの情報をget
		List<Position> positions = new PositionService().getName();
		request.setAttribute("positions", positions);
		//System.out.println(positions.size());
		
		//getした値をフォワード
		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		
		List<String> messages = new ArrayList<String>();
		
		User user = new User();
		user.setName(request.getParameter("name"));
		user.setLoginId(request.getParameter("loginId"));
		user.setPassword(request.getParameter("password"));
		user.setStoreId(Integer.parseInt(request.getParameter("storeId")));
		user.setPositionId(Integer.parseInt(request.getParameter("positionId")));
		
		List<Store> stores = new StoreService().getName();
		List<Position> positions = new PositionService().getName();
		

		//エラーメッセージ無かったら、ユーザー登録する
		if (isValid(request, messages) == true) {
			
			new UserService().register(user);
			
			//ユーザー管理画面へ遷移
			response.sendRedirect("userManagement");
			
		} else {
			//リストをjspにセットし、入力値が消えないようにする
			request.setAttribute("user", user);
			request.setAttribute("errorMessages", messages);
			request.setAttribute("stores", stores);
			request.setAttribute("positions", positions);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}
	}

	//バリデーション、エラーメッセージ
	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String name = request.getParameter("name");
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String passwordPre = request.getParameter("passwordPre");
		String storeId = request.getParameter("storeId");
		String positionId = request.getParameter("positionId");
		
		if (StringUtils.isEmpty(loginId) == true) {
			messages.add("ログインIDを入力してください");
		}else if(!(loginId.matches("^[a-zA-Z0-9]+$"))){
			messages.add("ログインIDを半角英数字で入力してください");
		}else if (loginId.length() < 6 || 20 < loginId.length()) {
			messages.add("ログインIDを6文字以上20文字以下で入力してください");
		}
		
		ManagementService managementService = new ManagementService();
		User loginIdCheckedUser = managementService.getLoginIdCheckedUser(loginId);
		if(loginIdCheckedUser != null){
			messages.add("登録済みのログインID ("+ loginIdCheckedUser.getName() + "さんのログインID)です");
		}
		
		if (StringUtils.isEmpty(password) == true) {
			messages.add("パスワードを入力してください");
		}else if(!(password.matches("^(?=.*?[0-9a-zA-Z])(?=.*?[!-/@_])[A-Za-z!-9@_]+$"))){
			messages.add("パスワードを記号を含む半角英数字で入力してください");
		}else if (password.length() < 6 || 20 < password.length()) {
			messages.add("パスワードを6文字以上20文字以下で入力してください");
		}
		
		if (!(password.equals(passwordPre))) {
			messages.add("パスワードと確認用パスワードが同一ではありません。");
		}
		
		if (StringUtils.isEmpty(name) == true) {
			messages.add("名前を入力してください");
		}else if (name.length() > 10) {
			messages.add("名前を10文字以下で入力してください");
		}
		
		if (StringUtils.isEmpty(storeId) == true) {
			messages.add("店舗を選択してください");
		}
		if (StringUtils.isEmpty(positionId) == true) {
			messages.add("部署・役職を選択してください");
		}
		
		if (messages.size() == 0) {
			return true;
		} else {
			// TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
			
			return false;
		}
	}

}
