package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserManagement;
import service.ManagementService;

@WebServlet(urlPatterns = { "/userManagement" }) //
public class UserManagementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,HttpServletResponse response) 
	throws IOException, ServletException {
		
		//投稿記事全てを表示
		List<UserManagement> usersManagement = new ManagementService().getUserManagements();
		request.setAttribute("usersManagement", usersManagement);
		
		//userManagement(この画面へ移動するリンク押された時に値をここに持ってきてる)
		request.getRequestDispatcher("/userManagement.jsp").forward(request, response);
		
		
		
	}	
}
