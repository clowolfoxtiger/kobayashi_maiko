package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import service.ManagementService;
import service.UserService;

@WebServlet(urlPatterns = { "/stopUser" })
public class StopUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	//停止するボタン押されたら
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();
		String userId = request.getParameter("userId");
		HttpSession session = request.getSession();
		
			try {
				ManagementService managementService = new ManagementService();
				User stopUser = managementService.getUser(userId);	
				request.setAttribute("stopUser", stopUser );
				
//				サーブレットでダイアログ出す方法
//				JFrame frame = new JFrame();
//				JOptionPane.showMessageDialog(frame, stopUser.getName() +"さんのアカウントを停止させますか？");
				
				new UserService().updateOne(stopUser);
				
			} catch (NoRowsUpdatedRuntimeException e) {
				session.removeAttribute("stopUser");
				messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
				response.sendRedirect("userManagement");
			}
			response.sendRedirect("userManagement");
	}
	
//	public static void showDialog(String msg){
//		JOptionPane.showMessageDialog(null, msg);
//	}
	
}
