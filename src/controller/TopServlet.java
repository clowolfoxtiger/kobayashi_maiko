package controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserComment;
import beans.UserNews;
import service.CommentService;
import service.UserNewsService;

@WebServlet(urlPatterns = { "/index.jsp" }) //
public class TopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,HttpServletResponse response) 
	throws IOException, ServletException {
		
		//ログイン済みユーザーをuserとしてset
		HttpSession session = request.getSession();
		List<String> messages = new ArrayList<String>();
		String category = request.getParameter("category");
		
		//入力された日時の取得
		Date d = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dateNow = sdf.format(d);
		String dateFirst = "2017-01-01 00:00:00";
		String dateStart = request.getParameter("dateStart") + " 00:00:00";
		String dateEnd = request.getParameter("dateEnd")+ " 23:59:59";
		if(request.getParameter("dateStart") == null || request.getParameter("dateStart") == "") {
			dateStart = dateFirst;
		}
		if (request.getParameter("dateEnd") == null || request.getParameter("dateEnd") == "") {
			dateEnd = dateNow;
		}
		
		//日付逆の時入れ替える処理
		//stringをdateにして比較
		Date startDate = null;
		Date endDate = null;
		try {
			startDate = sdf.parse(dateStart);//date型の引数
		} catch (ParseException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		
		try {
			endDate = sdf.parse(dateEnd);
		} catch (ParseException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		if(endDate.compareTo(startDate) == -1){
			dateStart = request.getParameter("dateEnd") + " 00:00:00";
			dateEnd = request.getParameter("dateStart") + " 23:59:59" ;
		}
		
		if (isValid(request, messages, dateStart, dateEnd, category) == true) {
			
			if(endDate.compareTo(startDate) == -1){
				request.setAttribute("dateStart",request.getParameter("dateEnd"));
				request.setAttribute("dateEnd", request.getParameter("dateStart"));
			}else{
				request.setAttribute("dateStart",request.getParameter("dateStart") );
				request.setAttribute("dateEnd", request.getParameter("dateEnd"));
			}
			
			request.setAttribute("category", category);
			
			//投稿リストをjspにセット
			List<UserNews> userNews = new UserNewsService().getUserNews(dateStart, dateEnd, category);
			request.setAttribute("userNews", userNews);
			
			//コメントリストをjspにセット
			List<UserComment> userComments = new CommentService().getUserComments();
			//System.out.println(userComments.get(0).getCreatedAt());
			request.setAttribute("userComments", userComments);
			
			//値をフォワード
			request.getRequestDispatcher("/top.jsp").forward(request, response);
		}else{
			
			if(endDate.compareTo(startDate) == -1){
				request.setAttribute("dateStart",request.getParameter("dateEnd"));
				request.setAttribute("dateEnd", request.getParameter("dateStart"));
				
			}else{
				request.setAttribute("dateStart",request.getParameter("dateStart"));
				request.setAttribute("dateEnd", request.getParameter("dateEnd"));
			}
			request.setAttribute("category", category);
			session.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("/top.jsp").forward(request, response);
		}
	}
	
	private boolean isValid(HttpServletRequest request, List<String> messages, String dateStart, String dateEnd, String category) {
	
		List<UserNews> userNews = new UserNewsService().getUserNews(dateStart, dateEnd, category);
		if (userNews == null) {
			messages.add("該当する記事はありません");
		}
		
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}

