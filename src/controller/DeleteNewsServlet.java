package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Comento;
import beans.News;
import service.CommentService;
import service.NewsService;

@WebServlet(urlPatterns = { "/deleteNews" })

public class DeleteNewsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		
		//投稿をdeleteする
		News news = new News();
		String newsId = request.getParameter("newsId");
		new NewsService().delete(news, newsId);
		
		//コメントをdeleteする
		Comento comment = new Comento();
		new CommentService().deleteAllComment(comment, newsId);
		
		//ホーム画面へredirect
		response.sendRedirect("./");
			
	}
}