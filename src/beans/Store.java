package beans;

import java.io.Serializable;

public class Store implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private int id;
	private String name;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	//Nameカラムの値をgetするメソッド
	public String getName() {
		return name;
	}

	//Nameカラムの値をputするメソッド
	public void setName(String name) {
		this.name = name;
	}

}