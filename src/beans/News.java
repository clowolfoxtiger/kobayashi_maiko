package beans;

import java.io.Serializable;
import java.util.Date;

public class News implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private int id;
	private String title;
	private String category;
	private String text;
	private int userId;
	private Date createdAt;
	private Date updatedAt;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	//Nameカラムの値をgetするメソッド
	public String getTitle() {
		return title;
	}

	//Nameカラムの値をputするメソッド
	public void setTitle(String title) {
		this.title = title;
	}
	
	//Nameカラムの値をgetするメソッド
	public String getCategory() {
		return category;
	}

	//Nameカラムの値をputするメソッド
	public void setCategory(String category) {
		this.category = category;
	}
		
	//Nameカラムの値をgetするメソッド
	public String getText() {
		return text;
	}

	//Nameカラムの値をputするメソッド
	public void setText(String text) {
		this.text = text;
	}
	
	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

}
