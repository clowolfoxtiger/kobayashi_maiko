package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;

@WebFilter({"/userManagement","/stopUser","/signup","/editUser"})
public class HeadOfficeAffairsFilter implements Filter {
	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		
		//前処理
		
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		try {
			User loginUser = (User)req.getSession().getAttribute("loginUser");
			if(loginUser == null || loginUser.getStoreId() == 1 && loginUser.getPositionId() == 1){ 
				//filterを実行
				chain.doFilter(request, response);
			}else{
				List<String> messages = new ArrayList<String>();
				messages.add("本社総務部以外はアクセスできません");
				req.getSession().setAttribute("errorMessages", messages);
				res.sendRedirect("./");
			}
		} finally {
			//後処理
			//System.out.println("LoginFilter end");
		}
	}

	@Override //doFilter()メソッドを実行するために書いておかねばならない
	public void init(FilterConfig config) {
	}

	@Override //doFilter()メソッドを実行するために書いておかねばならない
	public void destroy() {
	}

}
