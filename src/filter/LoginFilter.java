package filter;
	
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;

@WebFilter("/*")
public class LoginFilter implements Filter {
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		
		//前処理
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		String svlps = req.getServletPath();
		try {
			if( !svlps.equals("/login")){
				User user = (User)req.getSession().getAttribute("loginUser");
				if( svlps.equals("/css/style.css")){
					//サーブレットを実行
					chain.doFilter(request, response);
				}else if(user == null){
					//エラーメッセージ
					List<String> messages = new ArrayList<String>();
					messages.add("ログインしてください");
					req.getSession().setAttribute("errorMessages", messages);
					System.out.println("☆ログインしろ");
					System.out.println(svlps);

					res.sendRedirect("http://localhost:8080/kobayashi_maiko/login");
					return;
				}else{
					System.out.println(1);
					chain.doFilter(request, response);
				}
			}else{
				System.out.println(svlps);
				System.out.println(2);
				//サーブレットを実行
				chain.doFilter(request, response);
			}
		} finally {
			//後処理
			//System.out.println("LoginFilter end");
		}
	}

	@Override //doFilter()メソッドを実行するために書いておかねばならない
	public void init(FilterConfig config) {
	}

	@Override //doFilter()メソッドを実行するために書いておかねばならない
	public void destroy() {
	}

}
