package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Store;
import exception.SQLRuntimeException;

public class StoreDao {
	
	
	public void insert(Connection connection, Store store) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO stores ( ");
			sql.append("name");
			sql.append(") VALUES (");
			sql.append(" ?"); // name
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, store.getName());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	
	
	//storesテーブルの値を取る
	public List<Store> getStore(Connection connection ) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM stores");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<Store> ret = toStoreList(rs);
			
			//System.out.println(ret.size());
			
			
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	
	//storeListに値を追加
	private List<Store> toStoreList(ResultSet rs)
		throws SQLException {

		List<Store> ret = new ArrayList<Store>();
		try {
			while (rs.next()) { ///nextの意味
				int id = rs.getInt("id");
				String name = rs.getString("name");
				
				//System.out.println(name);
				
				Store store = new Store();
				store.setName(name);
				store.setId(id);
			
				ret.add(store);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}
