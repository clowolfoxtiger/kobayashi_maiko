package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.User;
import beans.UserManagement;
import exception.SQLRuntimeException;

public class ManagementDao {
	public List<UserManagement> getUserManagements(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			
			//user,store,positionのinnerjoin
			sql.append("SELECT * FROM users inner join stores on users.store_id = stores.id inner join positions on users.position_id = positions.id");
			sql.append(" ORDER BY users.created_at DESC");
			ps = connection.prepareStatement(sql.toString());
			System.out.println(sql.toString());

			ResultSet rs = ps.executeQuery(); 
			List<UserManagement> ret = toUserManagementsList(rs);
			return ret;
			
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	
	private List<UserManagement> toUserManagementsList(ResultSet rs)
			throws SQLException {

		List<UserManagement> ret = new ArrayList<UserManagement>();
		try {
			while (rs.next()) {
				int userId = rs.getInt("users.id");
				String userName = rs.getString("users.name");
				String loginId = rs.getString("login_id");
				String storeName = rs.getString("stores.name");
				int positionId = rs.getInt("positions.id");
				String positionName = rs.getString("positions.name");
				Timestamp updatedAt = rs.getTimestamp("users.updated_at");
				int isDeleted = rs.getInt("is_deleted");

				UserManagement userManagement = new UserManagement();
				userManagement.setUserId(userId);
				userManagement.setUserName(userName);
				userManagement.setLoginId(loginId);
				userManagement.setStoreName(storeName);
				userManagement.setPositionId(positionId);
				userManagement.setPositionName(positionName);
				userManagement.setUpdatedAt(updatedAt);
				userManagement.setIsDeleted(isDeleted);
				
				ret.add(userManagement);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
	
	
	
	//editUser 選択しているユーザーのみリストする
	public User getUser(Connection connection, String userId) {/////////////////////////////////////////////

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			
			//user,store,positionのinnerjoin
			sql.append("SELECT * FROM users WHERE users.id = ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1,userId);

			System.out.println(ps.toString());
			ResultSet rs = ps.executeQuery(); 
			List<User> userList = toUserList(rs);			
			
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	
	private List<User> toUserList(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				//System.out.println("ww");
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String loginId = rs.getString("login_id");
				String password = rs.getString("password");
				int storeId = rs.getInt("store_id");
				int positionId = rs.getInt("position_id");
				Timestamp createdAt = rs.getTimestamp("created_at");
				Timestamp updatedAt = rs.getTimestamp("updated_at");
				int isDeleted = rs.getInt("is_deleted");
//				byte[] icon = getIcon(rs);
				
				//System.out.println("あ" + storeId);

				User user = new User();
				user.setId(id);
				user.setName(name);
				user.setLoginId(loginId);
				user.setPassword(password);
				user.setStoreId(storeId);
				user.setPositionId(positionId);
//				user.setIcon(icon);
				user.setCreatedAt(createdAt);
				user.setUpdatedAt(updatedAt);
				user.setIsDeleted(isDeleted);
				ret.add(user);
				
				//System.out.println("あ" + storeId);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
	
	//editUser 選択しているユーザーのみリストする
		public User getLoginIdCheckedUser(Connection connection, String loginId) {/////////////////////////////////////////////

			PreparedStatement ps = null;
			try {
				StringBuilder sql = new StringBuilder();
				
				//user,store,positionのinnerjoin
				sql.append("SELECT * FROM users WHERE users.login_id = ?");

				ps = connection.prepareStatement(sql.toString());
				ps.setString(1,loginId);

				ResultSet rs = ps.executeQuery(); 
				List<User> userList = toLoginIdCheckedUserList(rs);			
				
				if (userList.isEmpty() == true) {
					return null;
				} else if (2 <= userList.size()) {
					throw new IllegalStateException("2 <= userList.size()");
				} else {
					return userList.get(0);
				}
			} catch (SQLException e) {
				throw new SQLRuntimeException(e);
			} finally {
				close(ps);
			}
		}
		
		private List<User> toLoginIdCheckedUserList(ResultSet rs) throws SQLException {

			List<User> ret = new ArrayList<User>();
			try {
				while (rs.next()) {
					int id = rs.getInt("id");
					String name = rs.getString("name");
					String loginId = rs.getString("login_id");
					String password = rs.getString("password");
					int storeId = rs.getInt("store_id");
					int positionId = rs.getInt("position_id");
					Timestamp createdAt = rs.getTimestamp("created_at");
					Timestamp updatedAt = rs.getTimestamp("updated_at");
					int isDeleted = rs.getInt("is_deleted");
					

					User user = new User();
					user.setId(id);
					user.setName(name);
					user.setLoginId(loginId);
					user.setPassword(password);
					user.setStoreId(storeId);
					user.setPositionId(positionId);
//					user.setIcon(icon);
					user.setCreatedAt(createdAt);
					user.setUpdatedAt(updatedAt);
					user.setIsDeleted(isDeleted);
					ret.add(user);
					
				}
				return ret;
			} finally {
				close(rs);
			}
		}

}
