package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.News;
import exception.SQLRuntimeException;

public class NewsDao {

	public void insert(Connection connection, News news) {//////////////////////////////////////////

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO news ( ");
			sql.append("title");
			sql.append(", category");
			sql.append(", text");
			sql.append(", user_id");
			sql.append(", created_at");
			sql.append(", updated_at");
			sql.append(") VALUES (");
			sql.append("?"); // title
			sql.append(", ?"); // category
			sql.append(", ?"); // text
			sql.append(", ?"); //user_id
			sql.append(", CURRENT_TIMESTAMP"); // created_at
			sql.append(", CURRENT_TIMESTAMP"); // updated_date
			sql.append(")");
			
			//sql文発行の準備
			ps = connection.prepareStatement(sql.toString());
			
			//?に値をセット
			ps.setString(1, news.getTitle());
			ps.setString(2, news.getCategory());
			ps.setString(3, news.getText());
			ps.setInt(4, news.getUserId());
			
			System.out.println(ps.toString());
			
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	
	
	public List<News> getNews(Connection connection) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM news";

			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			return toNewsList(rs);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	
	private List<News> toNewsList(ResultSet rs) throws SQLException {

		List<News> ret = new ArrayList<News>();
		try {
			while (rs.next()) {
				//System.out.println("ww");
				int id = rs.getInt("id");
				String title = rs.getString("title");
				String category = rs.getString("category");
				String text = rs.getString("text");
				int userId = rs.getInt("user_id");
				Timestamp createdAt = rs.getTimestamp("created_at");
				Timestamp updatedAt = rs.getTimestamp("updated_at");
				
//					byte[] icon = getIcon(rs);
				
				System.out.println("あ" + category);

				News news = new News();
				news.setId(id);
				news.setTitle(title);
				news.setCategory(category);
				news.setText(text);
				news.setUserId(userId);
//					user.setIcon(icon);
				news.setCreatedAt(createdAt);
				news.setUpdatedAt(updatedAt);
				
				ret.add(news);
				
				System.out.println("あ" + id); //
			}
			return ret;
		} finally {
			close(rs);
		}
	}
		
	public void update(Connection connection, News news) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE news SET");
			sql.append("  title = ?");
			sql.append(", category = ?");
			sql.append(", text = ?");
			sql.append(", user_id = ?");
			sql.append(", updated_at = CURRENT_TIMESTAMP");
//			if (user.getIcon() != null) {
//					sql.append(", icon = ?");
//				}

			sql.append(" WHERE");
			sql.append(" id = ?");
			sql.append(" AND");
			sql.append(" updated_at = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, news.getTitle());
			ps.setString(2, news.getCategory());
			ps.setString(3, news.getText());
			ps.setInt(4, news.getUserId());

//				if (user.getIcon() == null) {
//					ps.setInt(6, user.getId());
//					ps.setTimestamp(7,
//							new Timestamp(user.getUpdateDate().getTime()));
//				} else {
//					ps.setBinaryStream(6, new ByteArrayInputStream(user.getIcon()));
//					ps.setInt(7, user.getId());
//					ps.setTimestamp(8,
//							new Timestamp(user.getUpdateDate().getTime()));
//				}
//				int count = ps.executeUpdate();
//				if (count == 0) {
//					throw new NoRowsUpdatedRuntimeException();
//				}
//
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}

	//newsデリート
	public void delete(Connection connection, News news, String newsId) {///使ってる

		PreparedStatement ps = null;
		try {
			
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE FROM news WHERE id = ? ;");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, newsId);
			
			System.out.println(newsId);
			System.out.println(ps.toString());
			
			//実行sql文
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	

}
