package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.UserNews;
import exception.SQLRuntimeException;

public class UserNewsDao {
	
	public List<UserNews> getUserNews(Connection connection, String dateStart, String dateEnd, String category) {

		PreparedStatement ps = null;
		
		try {
			StringBuilder sql = new StringBuilder();
			
			sql.append("SELECT * FROM news inner join users on news.user_id = users.id WHERE news.created_at >= ? AND news.created_at <= ?");
			
			if(category != null && category != ""){
				sql.append(" AND category LIKE ?");
			}
			sql.append("ORDER BY news.created_at DESC");
			
			ps = connection.prepareStatement(sql.toString());
			
			ps.setString(1,dateStart);
			ps.setString(2,dateEnd);
			
			if(category != null && category != ""){
				ps.setString(3,"%"+category+"%");
			}

			ResultSet rs = ps.executeQuery();
			List<UserNews> ret = toUserNewsList(rs);
			
			if (ret.isEmpty() == true) {
				return null;
			}else{
				return ret;
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	
	private List<UserNews> toUserNewsList(ResultSet rs)
			throws SQLException {

		List<UserNews> ret = new ArrayList<UserNews>();
		try {
			while (rs.next()) {
				String title = rs.getString("title");
				String text = rs.getString("news.text");
				Timestamp createdAt = rs.getTimestamp("news.created_at");
				String category = rs.getString("news.category");
				String name = rs.getString("users.name");
				int newsId = rs.getInt("news.id");
				int userId = rs.getInt("users.id");
				
				UserNews userNews = new UserNews();
				userNews.setTitle(title);
				userNews.setText(text);
				userNews.setCreatedAt(createdAt);
				userNews.setCategory(category);
				userNews.setName(name);
				userNews.setNewsId(newsId);
				userNews.setUserId(userId);

				ret.add(userNews);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}
