package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;

public class UserDao {
	
	//ログインするユーザーを検索して、ユーザーリストに追加
	public User getUser(Connection connection, String loginId, String password) {////////////////////////////////////

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE login_id = ? AND password = ? ";

			ps = connection.prepareStatement(sql);
			ps.setString(1, loginId);
			ps.setString(2, password);
			
			System.out.println(ps.toString());
			ResultSet rs = ps.executeQuery();			
			List<User> userList = toUserList(rs);			
			
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 < userList.size()) {
				throw new IllegalStateException("2 < userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	//userListに値を追加
	private List<User> toUserList(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				//System.out.println("ww");
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String loginId = rs.getString("login_id");
				String password = rs.getString("password");
				int storeId = rs.getInt("store_id");
				int positionId = rs.getInt("position_id");
				Timestamp createdAt = rs.getTimestamp("created_at");
				Timestamp updatedAt = rs.getTimestamp("updated_at");
				int isDeleted = rs.getInt("is_deleted");
//				byte[] icon = getIcon(rs);

				User user = new User();
				user.setId(id);
				user.setName(name);
				user.setLoginId(loginId);
				user.setPassword(password);
				user.setStoreId(storeId);
				user.setPositionId(positionId);
//				user.setIcon(icon);
				user.setCreatedAt(createdAt);
				user.setUpdatedAt(updatedAt);
				user.setIsDeleted(isDeleted);
				ret.add(user);
				
			}
			return ret;
		} finally {
			close(rs);
		}
	}


/*	private byte[] getIcon(ResultSet rs) throws SQLException {
		byte[] ret = null;
		InputStream binaryStream = rs.getBinaryStream("icon");
		if (binaryStream != null) {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			StreamUtil.copy(binaryStream, baos);
			ret = baos.toByteArray();
		}
		return ret;
	}
*/
	
	//インサート文作成
	public void insert(Connection connection, User user) {///////////////////////////////////////

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
			sql.append("name");
			sql.append(", login_id");
			sql.append(", password");
			sql.append(", store_id");
			sql.append(", position_id");
			sql.append(", created_at");
			sql.append(") VALUES (");
			sql.append(" ?"); // name
			sql.append(", ?"); // login_id
			sql.append(", ?"); // password
			sql.append(", ?"); // store_id
			sql.append(", ?"); // position_id
			sql.append(", CURRENT_TIMESTAMP"); // created_at
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());
						
			ps.setString(1, user.getName());
			ps.setString(2, user.getLoginId());
			ps.setString(3, user.getPassword());
			ps.setInt(4, user.getStoreId());
			ps.setInt(5, user.getPositionId());
//			if (user.getIcon() == null) {
//				ps.setObject(6, null);
//			} else {
//				ps.setBinaryStream(6, new ByteArrayInputStream(user.getIcon()));
//			}
			
			System.out.println(ps.toString());

			
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void update(Connection connection, User editUser) {/////////////////////////////////////////
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append("  name = ?");//1
			sql.append(", login_id = ?");//2
			sql.append(", store_id = ?");//3
			sql.append(", position_id = ?");//4
			if(!StringUtils.isEmpty(editUser.getPassword())){
				sql.append(", password = ?");//5
			}

			sql.append(" WHERE");
			sql.append(" id = ?");//6,5
			//sql.append(" AND");
			//sql.append(" updated_at = ?");//7,6

			ps = connection.prepareStatement(sql.toString());
			
			ps.setString(1, editUser.getName());
			ps.setString(2, editUser.getLoginId());
			ps.setInt(3, editUser.getStoreId());
			ps.setInt(4, editUser.getPositionId());
			if(StringUtils.isEmpty(editUser.getPassword())){	
				System.out.println(editUser.getId());
				ps.setInt(5, editUser.getId());
//				ps.setTimestamp(6,new Timestamp(editUser.getUpdatedAt().getTime()));
			} else {
				System.out.println("ps.toString()d22");
				ps.setString(5, editUser.getPassword());
				ps.setInt(6, editUser.getId());
//				ps.setTimestamp(7,new Timestamp(editUser.getUpdatedAt().getTime()));
			}
			
			System.out.println(ps.toString());
			
			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
			
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}
	
	//ユーザー停止する
	public void updateOne(Connection connection, User stopUser) {//////////////////////////////////////////
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET is_deleted = 1 WHERE id = ?");

			ps = connection.prepareStatement(sql.toString());
			
			ps.setInt(1, stopUser.getId());
			
			//System.out.println(ps.toString());
			
			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
			
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	
	//ユーザー停止する
		public void updateZero(Connection connection, User reviveUser) {/////////////////////////////////////////
			PreparedStatement ps = null;
			try {
				StringBuilder sql = new StringBuilder();
				sql.append("UPDATE users SET is_deleted = 0 WHERE id = ?");

				ps = connection.prepareStatement(sql.toString());
				
				ps.setInt(1, reviveUser.getId());
				
				//System.out.println(ps.toString());
				
				int count = ps.executeUpdate();
				if (count == 0) {
					throw new NoRowsUpdatedRuntimeException();
				}
				
			} catch (SQLException e) {
				throw new SQLRuntimeException(e);
			} finally {
				close(ps);
			}
		}
}
