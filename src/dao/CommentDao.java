package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.Comento;
import beans.UserComment;
import exception.SQLRuntimeException;

public class CommentDao {
	
	//registerの処理を行う

	public void insert(Connection connection, Comento comment) {//////////////////////////////////////

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO comments ( ");
			sql.append("text");
			sql.append(", user_id");
			sql.append(", news_id");
			sql.append(", created_at");
			sql.append(", updated_at");
			sql.append(") VALUES (");
			sql.append("?"); // text 
			sql.append(", ?"); // userid
			sql.append(", ?"); // newsid
			sql.append(", CURRENT_TIMESTAMP"); // insert_date
			sql.append(", CURRENT_TIMESTAMP"); // update_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, comment.getComment());
			ps.setInt(2, comment.getUserId());
			ps.setInt(3, comment.getNewsId());
			
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	
	
	//Ajax　コメントインサート
	public int insertAjax(Connection connection, Comento comment) {//////////////////////////////////////

		PreparedStatement ps 	= null;
		ResultSet rs         	= null;
		int autoIncKey		= -1;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO comments ( ");
			sql.append("text");
			sql.append(", user_id");
			sql.append(", news_id");
			sql.append(", created_at");
			sql.append(", updated_at");
			sql.append(") VALUES (");
			sql.append("?"); // text 
			sql.append(", ?"); // userid
			sql.append(", ?"); // newsid
			sql.append(", CURRENT_TIMESTAMP"); // insert_date
			sql.append(", CURRENT_TIMESTAMP"); // update_date
			sql.append(")");
			
			ps = connection.prepareStatement(sql.toString(),
					java.sql.Statement.RETURN_GENERATED_KEYS);
			
			ps.setString(1, comment.getComment());
			ps.setInt(2, comment.getUserId());
			ps.setInt(3, comment.getNewsId());
			
			//追加
			ps.executeUpdate();
			System.out.println(ps.toString());
			//auto-incrementの値取得
			
			rs = ps.getGeneratedKeys();///ここでエラー
			System.out.println(rs);
			
			if (rs.next()) {
				autoIncKey = rs.getInt(1);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}   
			}
				//statementクローズ
			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}  
		}
		return autoIncKey;
	}
	
	

	//Ajax ComentoClass取得
	public Comento getCommentClass(Connection connection, int id) {////////////////////////////////////

		PreparedStatement ps = null;
		try {
			String sql = "select * from comments where id = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);
			
			System.out.println(ps.toString());
			ResultSet rs = ps.executeQuery();			
			List<Comento> commentList = toComentoList(rs);			
			
			if (commentList.isEmpty() == true) {
				return null;
			} else if (2 < commentList.size()) {
				throw new IllegalStateException("2 < commentList.size()");
			} else {
				return commentList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	
	//Ajax commentList取得
	private List<Comento> toComentoList(ResultSet rs)
			throws SQLException {

		List<Comento> ret = new ArrayList<Comento>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				Timestamp createdAt = rs.getTimestamp("created_at");

				Comento comment = new Comento();
				comment.setId(id);
				comment.setCreatedAt(createdAt);
				ret.add(comment);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
		
		
		
		
		
	
	
	public void delete(Connection connection, Comento comment, String commentId) {////////////////////////////////

		PreparedStatement ps = null;
		try {
			
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE FROM comments WHERE id = ? ;");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, commentId);
			
			//実行sql文
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	
	//news削除に伴うcomment全件削除
	public void deleteAllComment(Connection connection, Comento comment, String newsId) {///////////////////////

		PreparedStatement ps = null;
		try {
			
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE FROM comments WHERE news_id = ? ;");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, newsId);
			
			System.out.println(newsId);
			System.out.println(ps.toString());
			
			//実行sql文
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}


	public List<UserComment> getUserComments(Connection connection, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			
			//user,commentのinnerjoin
			sql.append("SELECT * FROM comments inner join users on comments.user_id = users.id");
			
			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery(); 
			List<UserComment> ret = toUserCommentsList(rs);
			return ret;
			
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	
	private List<UserComment> toUserCommentsList(ResultSet rs)
			throws SQLException {

		List<UserComment> ret = new ArrayList<UserComment>();
		try {
			while (rs.next()) {
				String name = rs.getString("name");
				String comment = rs.getString("text");
				int userId = rs.getInt("user_id");
				int newsId = rs.getInt("news_id");
				int commentId = rs.getInt("comments.id");
				Timestamp createdAt = rs.getTimestamp("comments.created_at");

				UserComment userComment = new UserComment();
				userComment.setName(name);
				userComment.setComment(comment);
				userComment.setUserId(userId);
				userComment.setNewsId(newsId);
				userComment.setCommentId(commentId);
				userComment.setCreatedAt(createdAt);
				ret.add(userComment);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}